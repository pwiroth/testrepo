﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.7">
  <POU Name="Feed_putElementConfigFB" Id="{a3f6f48a-129b-42bd-b902-bc2804562661}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK Feed_putElementConfigFB

VAR_INPUT
	I_CalledByBlock: ST_Block;
	I_Designation: STRING(_Constant._C_Lu_MaxDesigLength);
	I_HostDesig: STRING(_Constant._C_Lu_MaxDesigLength);
	I_Area: INT;
	I_DataLineDesignation: STRING(_Constant._C_Lu_MaxDesigLength) := ''; // OPTIONAL defaults to: '';
	I_PowerLineDesignation: STRING(_Constant._C_Lu_MaxDesigLength) := ''; // OPTIONAL defaults to: '';
	I_StLiDesig: STRING(_Constant._C_Lu_MaxDesigLength) := ''; // OPTIONAL defaults to: '';
	I_VID: INT := 0; // OPTIONAL defaults to: 0;
	I_Type: INT;
	I_HwIO: INT;
	I_DataLineType: INT := -1; // OPTIONAL defaults to: -1;
	I_PowerLineType: INT := -1; // OPTIONAL defaults to: -1;
	I_FeedDirection: INT := -1; // OPTIONAL defaults to: -1;
	I_HasLifebit: BOOL;
	I_SpeedType: INT;
	I_InduDesig: STRING(_Constant._C_Lu_MaxDesigLength) := ''; // OPTIONAL defaults to: '';
	I_InduInjectionPoint: REAL := 0.0; // In case of item being injected into this controls Indu, this is the position on the indu
	I_DropDesig: STRING(_Constant._C_Lu_MaxDesigLength) := ''; // OPTIONAL defaults to: '';
	I_DropAreaDesig: STRING(_Constant._C_Lu_MaxDesigLength) := ''; // OPTIONAL defaults to: '';
	I_MergDesig: STRING(_Constant._C_Lu_MaxDesigLength) := ''; // OPTIONAL defaults to: '';
	I_MfrhDesig: STRING(_Constant._C_Lu_MaxDesigLength) := ''; // OPTIONAL defaults to: '';
	I_MergPos: INT := -1; // OPTIONAL defaults to: -1;
	I_ControlLevel: INT := -1; // OPTIONAL defaults to: -1;
	I_MasterFeedDesig: STRING(_Constant._C_Lu_MaxDesigLength) := ''; // OPTIONAL defaults to: '';
	I_TimeoutTime: REAL;
	I_PhysicalDeviceType: INT;
	I_InductionNumber: INT := -1; // OPTIONAL defaults to: -1;
	I_ChuteNumber: INT := -1; // OPTIONAL defaults to: -1;
	I_ReverseInductionNo: INT := -1; // OPTIONAL defaults to: -1;
	I_InduFeedPosition: INT := -1; // OPTIONAL defaults to: -1;
	I_IncomingRTEorRTRDebounceTime: REAL := 0.0; // OPTIONAL defaults to: 0.0; Incoming signal debounce time
	I_OutgoingRTEorRTRDebounceTime: REAL := 0.0; // OPTIONAL defaults to: 0.0; Outgoing signal debounce time
END_VAR

VAR
	Block:		ST_Block;
	Desig:		ST_Lu_Desig_Element;
	UID:		INT;
	sTmpString:	STRING(_Constant._C_LstrLen);
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[Block.Name := 'Feed.putElementConfigFB'; _PLCInfo.Task[GETCURTASKINDEXEX()].LastCalledBlock := Block;

// INIT

// CONFIG

// MAIN
IF len(I_Designation) <= _Constant._C_Lu_MaxDesigLength THEN
	Desig.Desig := I_Designation;
	UID := Feed_getUIDByDesig(I_CalledByBlock := I_CalledByBlock, I_Desig := Desig);
	
	IF UID > -1 THEN
		_Feed.Objects[UID].writeElementConfig(
			I_CalledByBlock			:= Block,
			I_HostDesig				:= I_HostDesig,
			I_Area					:= I_Area,	
			I_UID					:= UID,
			I_DataLineDesignation	:= I_DataLineDesignation,
			I_PowerLineDesignation	:= I_PowerLineDesignation,
			I_VID					:= I_VID,
			I_Type					:= I_Type,
			I_HwIO					:= I_HwIO,
			I_DataLineType			:= I_DataLineType,
			I_PowerLineType			:= I_PowerLineType,
			I_FeedDirection 		:= I_FeedDirection,
			I_HasLifebit 			:= I_HasLifebit,
			I_SpeedType 			:= I_SpeedType,
			I_InduDesig 			:= I_InduDesig,
			I_InduInjectionPoint := I_InduInjectionPoint,
			I_DropDesig				:= I_DropDesig,
			I_DropAreaDesig			:= I_DropAreaDesig,
			I_MergDesig				:= I_MergDesig,
			I_MergPos				:= I_MergPos,
			I_ControlLevel 			:= I_ControlLevel,
			I_MasterFeedDesig 		:= I_MasterFeedDesig,
			I_TimeoutTime 			:= I_TimeoutTime,
			I_PhysicalDeviceType	:= I_PhysicalDeviceType,
			I_InductionNumber 		:= I_InductionNumber,
			I_ChuteNumber 			:= I_ChuteNumber,
			I_ReverseInductionNo 	:= I_ReverseInductionNo,
			I_InduFeedPosition		:= I_InduFeedPosition,
			I_MfrhDesig				:= I_MfrhDesig,
			I_StLiDesig				:= I_StLiDesig,
			I_IncomingRTEorRTRDebounceTime := I_IncomingRTEorRTRDebounceTime,
			I_OutgoingRTEorRTRDebounceTime := I_OutgoingRTEorRTRDebounceTime
		);
	ELSE
		sTmpString := CONCAT(STR1 := 'CONFIGURATION ERROR invalid UID for Desig ', STR2 := I_Designation);

		WriteLogMsg(
			I_Block			:= Block,
			I_CalledByBlock	:= I_CalledByBlock,
			I_MsgLevel		:= EN_LogLevel.Error,
			I_ItemUID		:= -1,
			I_ConUID		:= -1,
			I_ComPUID		:= -1,
			I_ConvUID		:= -1,
			I_CarrierUID	:= -1,
			I_DropUID		:= -1,
			I_Msg			:= sTmpString
		);
	END_IF
ELSE
	sTmpString := CONCAT(STR1 := 'CONFIGURATION ERROR max Desiglength ', STR2 := INT_TO_STRING(_Constant._C_Lu_MaxDesigLength));

	WriteLogMsg(
		I_Block			:= Block,
		I_CalledByBlock	:= I_CalledByBlock,
		I_MsgLevel		:= EN_LogLevel.Error,
		I_ItemUID		:= -1,
		I_ConUID		:= -1,
		I_ComPUID		:= -1,
		I_ConvUID		:= -1,
		I_CarrierUID	:= -1,
		I_DropUID		:= -1,
		I_Msg			:= sTmpString
	);
END_IF


I_Designation := '';
I_HostDesig := '';
I_Area := -1;
I_DataLineDesignation := '';
I_PowerLineDesignation := '';
I_StLiDesig := '';
I_VID := -1;
I_Type := -1;
I_HwIO := -1;
I_DataLineType := -1;
I_PowerLineType := -1;
I_FeedDirection := -1;
I_HasLifebit := FALSE;
I_SpeedType := -1;
I_InduDesig := '';
I_InduInjectionPoint := 0.0;
I_DropDesig := '';
I_DropAreaDesig := '';
I_MergDesig := '';
I_MfrhDesig := '';
I_MergPos := -1;
I_ControlLevel := -1;
I_MasterFeedDesig := '';
I_TimeoutTime := 0.0;
I_PhysicalDeviceType := -1;
I_InductionNumber := -1; 
I_ChuteNumber := -1; 
I_ReverseInductionNo := -1;
I_InduFeedPosition := -1;]]></ST>
    </Implementation>
    <LineIds Name="Feed_putElementConfigFB">
      <LineId Id="99" Count="41" />
      <LineId Id="275" Count="1" />
      <LineId Id="141" Count="34" />
      <LineId Id="205" Count="28" />
      <LineId Id="9" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>