﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.6">
  <POU Name="MsgPrtclSick_receive" Id="{a1640329-1b29-40d2-be3e-a74036ce6f73}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION MsgPrtclSick_receive : INT
 VAR_INPUT 
      I_CalledByBlock:  ST_Block;
	  I_ConUID:			INT;
END_VAR

VAR
    Block:              ST_Block;
    stWorkingMsg:       ST_MsgRaw;
    stWorkingMsgTmp:    ST_MsgRaw;

    Identifier_STX:     BYTE;
    Identifier_ETX:     BYTE;
    CountBytesReceived: INT;
    MessageMaxLength:   INT;
    iIndex:              INT;
    Finished:           BOOL;

    PosSTX:             INT;
    PosETX:             INT;
    RubbishBytes:       STRING;
    HeaderLength:       INT;
    LengthMsg:          INT;
    readRxBuffer:       BOOL;
    RxDataPosStart:     INT;
    RxDataPosEnd:       INT;
    RxDataCollected:    INT;
    sTmpString:         STRING(_Constant._C_LstrLen);
	iTmp:				INT;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[//Elementary tags of all Blocks
Block.Name := 'MsgPrtclSICK_receive'; _PLCInfo.Task[GETCURTASKINDEXEX()].LastCalledBlock := Block;
// Get Msg from Buffer
stWorkingMsg:= _Conn.Objects[I_ConUID].RAM.Buffer_Rx_RAW.GetHead();
_Conn.Objects[I_ConUID].RAM.Buffer_Rx_RAW.RemoveHead();

//get the count of received bytes
CountBytesReceived := stWorkingMsg.iLength;

//init some values
Identifier_STX := 16#02; // «STX»
Identifier_ETX := 16#03; // «ETX»
MessageMaxLength := 1024;
HeaderLength := 2; 

//get the content of receive buffer to local
IF CountBytesReceived > 0 THEN
				
	//find STX and ETX 
	PosSTX := -1;
	PosETX := -1;
	FOR iIndex := 0 TO CountBytesReceived - 1 BY 1 DO
		IF stWorkingMsg.arrByte[iIndex] = Identifier_STX  AND PosSTX = -1 THEN
			PosSTX := iIndex;                       
		END_IF
		IF stWorkingMsg.arrByte[iIndex] = Identifier_ETX THEN
			PosETX := iIndex;
			EXIT;
		END_IF
	END_FOR
	//Skip on ETX before STX or one of them not found
	IF PosSTX > -1 AND PosETX > -1 AND PosSTX < PosETX THEN
		//Rubbish bytes? ---> At least, log'em!
		IF PosSTX > 0 THEN
			RubbishBytes := '';
			FOR iIndex := 0 TO PosSTX - 1 DO
				RubbishBytes := CONCAT(STR1 := RubbishBytes, STR2 := F_ToCHR(stWorkingmsg.arrByte[iIndex]));
			END_FOR;
			WriteLogMsg(  I_Block := Block,
						  I_CalledByBlock := I_CalledByBlock,
						  I_ItemUID := -1,
						  I_ConUID := I_ConUID,
						  I_MsgLevel := EN_LogLevel.Error,
						  I_ComPUID := -1,
						  I_ConvUID := -1,
						  I_CarrierUID := -1,
						  I_DropUID := -1,
						  I_Msg := CONCAT(STR1 := 'Going to ignore content of bytes received before STX Content was ', STR2 := RubbishBytes));                      
		END_IF;
		IF (LengthMsg := PosETX - PosSTX - 1) >= HeaderLength THEN
				//extract msg
				FOR iIndex := PosSTX + 1 TO PosETX - 1 DO
					stWorkingMsg.arrByte[iIndex - (PosSTX + 1)] := stWorkingMsg.arrByte[iIndex];
				END_FOR
				//remove trailing stuff
				FOR iIndex := 0 TO stWorkingMsg.iLength - LengthMsg - 1 BY 1 DO
					stWorkingMsg.arrByte[LengthMsg + iIndex] := 0;
				END_FOR
				//Put in current size
				stWorkingMsg.iLength := LengthMsg;
				IF I_ConUID > -1 THEN
					IF _Conn.Objects[I_ConUID].Config.EnableDebugLog THEN
						//Log		
						sTmpString := LONGCAT(I_CalledByBlock := Block, I_LSTR1 := 'Invoke SICK dispatcher with Message of ', I_LSTR2 := INT_TO_STRING(LengthMsg));
						sTmpString := LONGCAT(I_CalledByBlock := Block, I_LSTR1 := sTmpString, I_LSTR2 := ' bytes:  ');	
						iTmp := LLEN(I_CalledByBlock := Block, I_LSTR := sTmpString);			
						FOR iIndex := 0 TO LengthMsg - 1 BY 1 DO 
							sTmpString[iTmp + iIndex] := stWorkingMsg.arrByte[iIndex];
						END_FOR
						sTmpString[iTmp + iIndex] := 0;
						WriteLogMsg(  I_Block := Block,
									  I_CalledByBlock := I_CalledByBlock,
									  I_ItemUID := -1,
									  I_ConUID := I_ConUID,
									  I_MsgLevel := EN_LogLevel.Debug,
									  I_ComPUID := -1,
									  I_ConvUID := -1,
									  I_CarrierUID := -1,
									  I_DropUID := -1,
									  I_Msg := sTmpString
								);              
					END_IF;
			   	END_IF
			//now its time to call the dispatcher
			//***********************************
			MsgPrtclSick_dispatch(I_CalledByBlock := Block,
									 I_stMsg := stWorkingMsg, I_ConUID := I_ConUID);
			   
		ELSE //not enough bytes for a complete message
			sTmpString := CONCAT(STR1 := 'UID ', STR2 := INT_TO_STRING(_Conn.Objects[I_ConUID].Config.UID));
			sTmpString := CONCAT(STR1 :=sTmpString, STR2 :=' Message to dispatch is even shorter as the specified header. Or empty. Skipped.');
			WriteLogMsg(  I_Block := Block,
						  I_CalledByBlock := I_CalledByBlock,
						  I_ItemUID := -1,
						  I_ConUID := I_ConUID,
						  I_MsgLevel := EN_LogLevel.Error,
						  I_ComPUID := -1,
						  I_ConvUID := -1,
						  I_CarrierUID := -1,
						  I_DropUID := -1,
						  I_Msg := sTmpString
					  );
		END_IF;
	ELSE //no STX or ETX found or other weird stuff
		
		sTmpString := ' Received Message has weird STX/ETX constellation or one of them is even missing';
		WriteLogMsg(  I_Block := Block,
					  I_CalledByBlock := I_CalledByBlock,
					  I_ItemUID := -1,
					  I_ConUID := I_ConUID,
					  I_MsgLevel := EN_LogLevel.Error,
					  I_ComPUID := -1,
					  I_ConvUID := -1,
					  I_CarrierUID := -1,
					  I_DropUID := -1,
					  I_Msg := sTmpString
				  );
	END_IF		
END_IF]]></ST>
    </Implementation>
    <LineIds Name="MsgPrtclSick_receive">
      <LineId Id="3" Count="117" />
      <LineId Id="2" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>