﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.6">
  <POU Name="FB_InnerByteBuffer" Id="{fbea9bda-80dd-4fa8-b24c-e16b0e1ce28c}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_InnerByteBuffer
VAR_INPUT
	// TRUE => Enable log message output, FALSE => Disable 
	bLog: BOOL := TRUE;
    I_CalledByBlock: STRING(_Constant._C_Lu_MaxDesigLength);
END_VAR

VAR_OUTPUT
END_VAR

VAR
	Buffer: ST_FB_ByteBuffer_Threadsafe_InnerByteBuffer; // this is the inner byte buffer, where data is pulled from

    sTmpString: STRING;
    Block: ST_Block;
	bytesEverWrittenToBuffer: ULINT; // the absolute amount of bytes written to this buffer
	messagesEverWrittenToBuffer: ULINT; // the absolute amount of messages written to this buffer
	messagesEverRemovedFromBuffer: ULINT; // the absolute amount of messages read from this buffer
	
	// temporary (or maybe not?) status data
	DO_NOT_USE_EntriesAvailable: UDINT := 0;
	DO_NOT_USE_EntriesWaiting: UDINT := 0;
	DO_NOT_USE_BytesAvailable: UDINT := 0;
	DO_NOT_USE_BytesWaiting: UDINT := 0;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[]]></ST>
    </Implementation>
    <Method Name="AddTailAsMsgRaw" Id="{f194c55c-5f77-446c-b5a2-78feb6dfc0cd}">
      <Declaration><![CDATA[METHOD AddTailAsMsgRaw
VAR_INPUT
    I_CalledByBlock: ST_Block;
	Msg: ST_MsgRaw; (*TESTCOMMENT*)
END_VAR
VAR
    Block: ST_Block;
    sTmpString: STRING(_Constant._C_LstrLen);
	IndexToDeleteMessagesToFreeBytes: INT;
	EntryDataIndexToBeWrittenNext: UDINT;
	AvailableConsecutiveSpaceAfterTheLastWrittenByte: UDINT;
	MessageLength: UDINT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[Block.Name := 'FB_ByteBuffer.AddTailAsMsgRaw'; _PLCInfo.Task[GETCURTASKINDEXEX()].LastCalledBlock := Block;
MessageLength := INT_TO_UDINT(Msg.iLength);

// Check if Buffer is large enough to hold this message as a whole; is not, return
IF (_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Bytes - 1) >= MessageLength THEN

	// Check if Buffer has a free entry; if not, overwrite oldest entry (we only need one entry right now)
	IF EntriesAvailable < 1 THEN
		RemoveHead();
	END_IF

	// Check if Buffer has sufficient available bytes; if not, overwrite up to twenty of the oldest entries
	// "twenty" is an arbitrary number - a large message may take up the space of a lot of small ones though
	// With the very first check for buffer size we ensured the new message will fit into this empty buffer
	// @MG: YES, I HAVE STARTED AT "1"!
	FOR IndexToDeleteMessagesToFreeBytes := 1 TO 20 DO
		IF BytesAvailable < MessageLength THEN
			RemoveHead();
		ELSE
			EXIT;
		END_IF
	END_FOR
	
	// recheck whether the message now fits into the available buffer space
	IF BytesAvailable >= MessageLength THEN
		IF Buffer.IndexOfLastWrittenEntryData < (_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Entries - 1) THEN // as we are interested in the NEXT EntryDataIndex after the written EntryData, we check whether the index of the next EntryData is 0 or (IndexOfLastWrittenEntryData + 1)
			EntryDataIndexToBeWrittenNext := Buffer.IndexOfLastWrittenEntryData + 1;
		ELSE
			EntryDataIndexToBeWrittenNext := 0;
		END_IF
		
		// First we check for consecutive free space - everyone likes to put his data in consecutive space if possible
		AvailableConsecutiveSpaceAfterTheLastWrittenByte := (_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Bytes - 1) - Buffer.EntryData[Buffer.IndexOfLastWrittenEntryData].IndexOfLastByte;
		
		IF AvailableConsecutiveSpaceAfterTheLastWrittenByte > 0 THEN // In this case, we still have spare bytes to use until the end; we decide to use the next one after the last written message
			Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfFirstByte := Buffer.EntryData[Buffer.IndexOfLastWrittenEntryData].IndexOfLastByte + 1;
		ELSE // otherwise, we must be at the end of the buffer
			Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfFirstByte := 0;
		END_IF
		
		IF AvailableConsecutiveSpaceAfterTheLastWrittenByte >= MessageLength THEN // We decide whether we can write as consecutive byte stream or have to do it in two steps
			// For reading the data later, we mark whether this is a consecutive memory area
			Buffer.EntryData[EntryDataIndexToBeWrittenNext].consecutiveMemorySpace := TRUE;
			
			// This is the copy part itself
			MEMCPY(destAddr := ADR(Buffer.Element[Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfFirstByte]), srcAddr := ADR(Msg.arrByte[0]), n := MessageLength);
			Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfLastByte := Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfFirstByte + MessageLength - 1;
		ELSE
			// For reading the data later, we mark whether this is a consecutive memory area
			Buffer.EntryData[EntryDataIndexToBeWrittenNext].consecutiveMemorySpace := FALSE;
			
			// This is the copy part itself
			MEMCPY(destAddr := ADR(Buffer.Element[Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfFirstByte]), srcAddr := ADR(Msg.arrByte[0]), n := AvailableConsecutiveSpaceAfterTheLastWrittenByte);
			MEMCPY(destAddr := ADR(Buffer.Element[0]), srcAddr := ADR(Msg.arrByte[0 + AvailableConsecutiveSpaceAfterTheLastWrittenByte]), n := (MessageLength - AvailableConsecutiveSpaceAfterTheLastWrittenByte));
			Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfLastByte := MessageLength - AvailableConsecutiveSpaceAfterTheLastWrittenByte - 1;
		END_IF
		
		// Update MUST happen AFTER the data is written!
		Buffer.EntryData[EntryDataIndexToBeWrittenNext].writtenButNotRead := TRUE;
	
		// Update MUST happen AFTER the data is written!
		Buffer.IndexOfLastWrittenEntryData := EntryDataIndexToBeWrittenNext;
		
		// Update Statistics
		bytesEverWrittenToBuffer := bytesEverWrittenToBuffer + MessageLength;
		messagesEverWrittenToBuffer := messagesEverWrittenToBuffer + 1;
		
		// Update State values
		updateStates();
	ELSE // the message still does not fit into the buffer
		;
		(*
		sTmpString := LCONCAT('Could not add entry of length ', INT_TO_STRING(Msg.iLength));
		sTmpString := LCONCAT(sTmpString, ' to a buffer of size ');
		sTmpString := LCONCAT(sTmpString, UDINT_TO_STRING((_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Bytes - 1)));   
		sTmpString := LCONCAT(sTmpString, ' due to BytesAvailable ');
		sTmpString := LCONCAT(sTmpString, UDINT_TO_STRING(BytesAvailable));    	
		WriteLogMsg(
			I_Block			:= Block,
			I_CalledByBlock	:= I_CalledByBlock,
			I_MsgLevel		:= EN_LogLevel.Error,
			I_ItemUID		:= -1,
			I_ConUID		:= -1,
			I_ComPUID		:= -1,
			I_ConvUID		:= -1,
			I_CarrierUID	:= -1,
			I_DropUID		:= -1,
			I_Msg			:= sTmpString
		);
		*)
	END_IF
ELSE // buffer too small handler
	sTmpString := LCONCAT('Could not add entry of length ', INT_TO_STRING(Msg.iLength)); 
	sTmpString := LCONCAT(sTmpString, ' to a buffer of size '); 
	sTmpString := LCONCAT(sTmpString, UDINT_TO_STRING((_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Bytes - 1)));      	
	WriteLogMsg(
		I_Block			:= Block,
		I_CalledByBlock	:= I_CalledByBlock,
		I_MsgLevel		:= EN_LogLevel.Error,
		I_ItemUID		:= -1,
		I_ConUID		:= -1,
		I_ComPUID		:= -1,
		I_ConvUID		:= -1,
		I_CarrierUID	:= -1,
		I_DropUID		:= -1,
		I_Msg			:= sTmpString
	);
END_IF
]]></ST>
      </Implementation>
    </Method>
    <Method Name="AddTailAsPointerToByte" Id="{8f331fc7-740e-4f74-968c-6255aa714388}">
      <Declaration><![CDATA[METHOD AddTailAsPointerToByte
VAR_INPUT
    I_CalledByBlock: ST_Block;
	I_DataInNumberOfParts: UINT;
	I_PointerToByte_Part1: POINTER TO BYTE;
	I_DataLength_Part1: UDINT;
	I_PointerToByte_Part2: POINTER TO BYTE;
	I_DataLength_Part2: UDINT;
END_VAR
VAR
    Block: ST_Block;
    sTmpString: STRING(_Constant._C_LstrLen);
	IndexToDeleteMessagesToFreeBytes: INT;
	EntryDataIndexToBeWrittenNext: UDINT;
	AvailableConsecutiveSpaceAfterTheLastWrittenByte: UDINT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[Block.Name := 'FB_ByteBuffer.AddTailAsPointerToByte'; _PLCInfo.Task[GETCURTASKINDEXEX()].LastCalledBlock := Block;

// Check if Buffer is large enough to hold this message as a whole; is not, return
IF I_DataInNumberOfParts > 0 THEN
	IF (_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Bytes - 1) >= (I_DataLength_Part1 + I_DataLength_Part2) THEN
	
		// Check if Buffer has a free entry; if not, overwrite oldest entry (we only need one entry right now)
		IF EntriesAvailable < 1 THEN
			RemoveHead();
		END_IF
	
		// Check if Buffer has sufficient available bytes; if not, overwrite up to twenty of the oldest entries
		// "twenty" is an arbitrary number - a large message may take up the space of a lot of small ones though
		// With the very first check for buffer size we ensured the new message will fit into this empty buffer
		// @MG: YES, I HAVE STARTED AT "1"!
		FOR IndexToDeleteMessagesToFreeBytes := 1 TO 20 DO
			IF BytesAvailable < (I_DataLength_Part1 + I_DataLength_Part2) THEN
				RemoveHead();
			ELSE
				EXIT;
			END_IF
		END_FOR
		
		// recheck whether the message now fits into the available buffer space
		IF BytesAvailable >= (I_DataLength_Part1 + I_DataLength_Part2) THEN
			IF Buffer.IndexOfLastWrittenEntryData < (_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Entries - 1) THEN // as we are interested in the NEXT EntryDataIndex after the written EntryData, we check whether the index of the next EntryData is 0 or (IndexOfLastWrittenEntryData + 1)
				EntryDataIndexToBeWrittenNext := Buffer.IndexOfLastWrittenEntryData + 1;
			ELSE
				EntryDataIndexToBeWrittenNext := 0;
			END_IF
			
			// First we check for consecutive free space - everyone likes to put his data in consecutive space if possible
			AvailableConsecutiveSpaceAfterTheLastWrittenByte := (_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Bytes - 1) - Buffer.EntryData[Buffer.IndexOfLastWrittenEntryData].IndexOfLastByte;
			
			IF AvailableConsecutiveSpaceAfterTheLastWrittenByte > 0 THEN // In this case, we still have spare bytes to use until the end; we decide to use the next one after the last written message
				Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfFirstByte := Buffer.EntryData[Buffer.IndexOfLastWrittenEntryData].IndexOfLastByte + 1;
			ELSE // otherwise, we must be at the end of the buffer
				Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfFirstByte := 0;
			END_IF
			
			IF AvailableConsecutiveSpaceAfterTheLastWrittenByte >= (I_DataLength_Part1 + I_DataLength_Part2) THEN // We decide whether we can write as consecutive byte stream or have to do it in two steps
				// For reading the data later, we mark whether this is a consecutive memory area
				Buffer.EntryData[EntryDataIndexToBeWrittenNext].consecutiveMemorySpace := TRUE;
				
				// This is the copy part itself
				IF I_DataInNumberOfParts = 1 THEN
					MEMCPY(destAddr := ADR(Buffer.Element[Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfFirstByte]), srcAddr := I_PointerToByte_Part1, n := I_DataLength_Part1);
					Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfLastByte := Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfFirstByte + I_DataLength_Part1 - 1;
				ELSIF I_DataInNumberOfParts = 2 THEN
					MEMCPY(destAddr := ADR(Buffer.Element[Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfFirstByte]), srcAddr := I_PointerToByte_Part1, n := I_DataLength_Part1);				
					MEMCPY(destAddr := ADR(Buffer.Element[Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfFirstByte + I_DataLength_Part1]), srcAddr := I_PointerToByte_Part2, n := I_DataLength_Part2);
					Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfLastByte := Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfFirstByte + I_DataLength_Part1 - 1;
				ELSE // EXCEPTION
					;
				END_IF
			ELSE
				// For reading the data later, we mark whether this is a consecutive memory area
				Buffer.EntryData[EntryDataIndexToBeWrittenNext].consecutiveMemorySpace := FALSE;
				
				// This is the copy part itself
				IF I_DataInNumberOfParts = 1 THEN
					MEMCPY(destAddr := ADR(Buffer.Element[Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfFirstByte]), srcAddr := I_PointerToByte_Part1, n := AvailableConsecutiveSpaceAfterTheLastWrittenByte);
					MEMCPY(destAddr := ADR(Buffer.Element[0]), srcAddr := I_PointerToByte_Part1[AvailableConsecutiveSpaceAfterTheLastWrittenByte], n := ((I_DataLength_Part1 + I_DataLength_Part2) - AvailableConsecutiveSpaceAfterTheLastWrittenByte));
					Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfLastByte := (I_DataLength_Part1 + I_DataLength_Part2) - AvailableConsecutiveSpaceAfterTheLastWrittenByte - 1;
				ELSIF I_DataInNumberOfParts = 2 THEN
					IF AvailableConsecutiveSpaceAfterTheLastWrittenByte < I_DataLength_Part1 THEN
						// My available space is smaller than the first part of my data - I put as much data of the first part in as possible: AvailableConsecutiveSpaceAfterTheLastWrittenByte
						MEMCPY(destAddr := ADR(Buffer.Element[Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfFirstByte]), srcAddr := I_PointerToByte_Part1, n := AvailableConsecutiveSpaceAfterTheLastWrittenByte);
						// After the above, I still have (I_DataLength_Part1 - AvailableConsecutiveSpaceAfterTheLastWrittenByte) data left; I put this in the beginning of the buffer
						MEMCPY(destAddr := ADR(Buffer.Element[0]), srcAddr := I_PointerToByte_Part1[AvailableConsecutiveSpaceAfterTheLastWrittenByte], n := (I_DataLength_Part1 - AvailableConsecutiveSpaceAfterTheLastWrittenByte));
						
						// Now I have finished with the first part of my data, and stand at (Buffer.Element[(I_DataLength_Part1 - AvailableConsecutiveSpaceAfterTheLastWrittenByte)]) of the buffer, but part 2 of the data fits in the buffer now
						MEMCPY(destAddr := ADR(Buffer.Element[(I_DataLength_Part1 - AvailableConsecutiveSpaceAfterTheLastWrittenByte)]), srcAddr := I_PointerToByte_Part2, n := I_DataLength_Part2);
						
						Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfLastByte := (I_DataLength_Part1 + I_DataLength_Part2) - AvailableConsecutiveSpaceAfterTheLastWrittenByte - 1;
					ELSE
						// My available space is larger than the first part of my data - I put my first part in and as much data of the second part as possible: AvailableConsecutiveSpaceAfterTheLastWrittenByte - I_DataLength_Part1
						MEMCPY(destAddr := ADR(Buffer.Element[Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfFirstByte]), srcAddr := I_PointerToByte_Part1, n := I_DataLength_Part1);
						// After the above, I still have (AvailableConsecutiveSpaceAfterTheLastWrittenByte - I_DataLength_Part1) space left; I put the beginning of the second part in there
						MEMCPY(destAddr := ADR(Buffer.Element[Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfFirstByte + I_DataLength_Part1]), srcAddr := I_PointerToByte_Part2, n := (AvailableConsecutiveSpaceAfterTheLastWrittenByte - I_DataLength_Part1));
						
						// Now I have finished with the first part of my data, and have written(AvailableConsecutiveSpaceAfterTheLastWrittenByte - I_DataLength_Part1) of the second part
						MEMCPY(destAddr := ADR(Buffer.Element[0]), srcAddr := I_PointerToByte_Part2[AvailableConsecutiveSpaceAfterTheLastWrittenByte - I_DataLength_Part1], n := (I_DataLength_Part2 - (AvailableConsecutiveSpaceAfterTheLastWrittenByte - I_DataLength_Part1)));
						
						Buffer.EntryData[EntryDataIndexToBeWrittenNext].IndexOfLastByte := (I_DataLength_Part1 + I_DataLength_Part2) - AvailableConsecutiveSpaceAfterTheLastWrittenByte - 1;
					END_IF
				ELSE // EXCEPTION
					;
				END_IF
			END_IF
			
			// Update MUST happen AFTER the data is written!
			Buffer.EntryData[EntryDataIndexToBeWrittenNext].writtenButNotRead := TRUE;
		
			// Update MUST happen AFTER the data is written!
			Buffer.IndexOfLastWrittenEntryData := EntryDataIndexToBeWrittenNext;
		
			// Update Statistics
			bytesEverWrittenToBuffer := bytesEverWrittenToBuffer + I_DataLength_Part1 + I_DataLength_Part2;
			messagesEverWrittenToBuffer := messagesEverWrittenToBuffer + 1;
			
			// Update State values
			updateStates();
		ELSE // the message still does not fit into the buffer
			;
			(*
			sTmpString := LCONCAT('Could not add entry of length ', INT_TO_STRING(Msg.iLength));
			sTmpString := LCONCAT(sTmpString, ' to a buffer of size ');
			sTmpString := LCONCAT(sTmpString, UDINT_TO_STRING((_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Bytes - 1)));   
			sTmpString := LCONCAT(sTmpString, ' due to BytesAvailable ');
			sTmpString := LCONCAT(sTmpString, UDINT_TO_STRING(BytesAvailable));    	
			WriteLogMsg(
				I_Block			:= Block,
				I_CalledByBlock	:= I_CalledByBlock,
				I_MsgLevel		:= EN_LogLevel.Error,
				I_ItemUID		:= -1,
				I_ConUID		:= -1,
				I_ComPUID		:= -1,
				I_ConvUID		:= -1,
				I_CarrierUID	:= -1,
				I_DropUID		:= -1,
				I_Msg			:= sTmpString
			);
			*)
		END_IF
	ELSE // buffer too small handler
		sTmpString := LCONCAT('Could not add entry of length ', UDINT_TO_STRING(I_DataLength_Part1 + I_DataLength_Part2)); 
		sTmpString := LCONCAT(sTmpString, ' to a buffer of size '); 
		sTmpString := LCONCAT(sTmpString, UDINT_TO_STRING((_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Bytes - 1)));      	
		WriteLogMsg(
			I_Block			:= Block,
			I_CalledByBlock	:= I_CalledByBlock,
			I_MsgLevel		:= EN_LogLevel.Error,
			I_ItemUID		:= -1,
			I_ConUID		:= -1,
			I_ComPUID		:= -1,
			I_ConvUID		:= -1,
			I_CarrierUID	:= -1,
			I_DropUID		:= -1,
			I_Msg			:= sTmpString
		);
	END_IF
ELSE // I_DataInNumberOfParts handler
	sTmpString := LCONCAT('Could not add entry I_DataInNumberOfParts ', UDINT_TO_STRING(I_DataInNumberOfParts)); 	
	WriteLogMsg(
		I_Block			:= Block,
		I_CalledByBlock	:= I_CalledByBlock,
		I_MsgLevel		:= EN_LogLevel.Error,
		I_ItemUID		:= -1,
		I_ConUID		:= -1,
		I_ComPUID		:= -1,
		I_ConvUID		:= -1,
		I_CarrierUID	:= -1,
		I_DropUID		:= -1,
		I_Msg			:= sTmpString
	);
END_IF
]]></ST>
      </Implementation>
    </Method>
    <Property Name="BytesAvailable" Id="{70c8791e-9365-4300-a7e6-56a7264e2272}">
      <Declaration><![CDATA[PROPERTY BytesAvailable : udint]]></Declaration>
      <Get Name="Get" Id="{d7e094bc-15f7-45e5-801d-159433e00258}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[BytesAvailable := DO_NOT_USE_BytesAvailable;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="BytesWaiting" Id="{d3a2f5c1-6c5a-47a8-9e0e-d5eae563fde6}">
      <Declaration><![CDATA[PROPERTY BytesWaiting : udint]]></Declaration>
      <Get Name="Get" Id="{679f8e07-f54b-4605-86e6-abe1a85ebab4}">
        <Declaration><![CDATA[VAR
	value: UDINT;
	IndexToBeReadNext: UDINT;
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[BytesWaiting := DO_NOT_USE_BytesWaiting;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Method Name="Clear" Id="{07feb7dc-8e25-4c84-9938-6bd7c95c4e74}">
      <Declaration><![CDATA[METHOD Clear 
VAR_INPUT
END_VAR
VAR
	tmpBuffer: ST_FB_ByteBuffer_Threadsafe_InnerByteBuffer;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[Buffer := tmpBuffer;
]]></ST>
      </Implementation>
    </Method>
    <Property Name="EntriesAvailable" Id="{cd606264-91a9-4df6-a4b6-953a2eb052c3}">
      <Declaration><![CDATA[PROPERTY EntriesAvailable : udint ]]></Declaration>
      <Get Name="Get" Id="{3dffaec4-e46a-49a2-b670-934768ed155d}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[EntriesAvailable := DO_NOT_USE_EntriesAvailable;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="EntriesWaiting" Id="{88fce33e-cac1-41f1-8b1d-6edaf9eeda4e}">
      <Declaration><![CDATA[PROPERTY EntriesWaiting : udint]]></Declaration>
      <Get Name="Get" Id="{739380aa-0342-4b41-93e3-475f10dcb8cc}">
        <Declaration><![CDATA[VAR
	value: UDINT;
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[EntriesWaiting := DO_NOT_USE_EntriesWaiting;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Method Name="GetHeadAsMsgRaw" Id="{8381de3d-a5db-46b3-8f1b-5d153dfa2fd7}">
      <Declaration><![CDATA[METHOD GetHeadAsMsgRaw : ST_MsgRaw
VAR_INPUT
END_VAR
VAR
	stMsg: ST_MsgRaw;
	EntryDataIndexToBeReadNext: UDINT;
	MessageLength: UDINT;
	MessageLengthOfFirstPart: UDINT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[//get the oldest entry
stMsg	:= GetEmptyRawMsg();

IF Buffer.IndexOfLastReadEntryData < (_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Entries - 1) THEN // as we are interested in the NEXT byte after the last read message, we check whether the index of the next EntryData is 0 or (IndexofLastEntryData + 1)
	EntryDataIndexToBeReadNext := Buffer.IndexOfLastReadEntryData + 1;
ELSE
	EntryDataIndexToBeReadNext := 0;
END_IF

IF Buffer.EntryData[EntryDataIndexToBeReadNext].writtenButNotRead THEN
	IF Buffer.EntryData[EntryDataIndexToBeReadNext].consecutiveMemorySpace THEN // depending on how the message is arranges in the buffer elements/ memory, we either can copy it as a whole at once, or must do a first and second part copy
		// get the message length
		MessageLength := Buffer.EntryData[EntryDataIndexToBeReadNext].IndexOfLastByte - Buffer.EntryData[EntryDataIndexToBeReadNext].IndexOfFirstByte + 1;
		
		// now populate the return
		stMsg.iLength := UDINT_TO_INT(MessageLength);
		MEMCPY(destAddr := ADR(stMsg.arrByte[0]), srcAddr := ADR(Buffer.Element[Buffer.EntryData[EntryDataIndexToBeReadNext].IndexOfFirstByte]), n := MessageLength);
	ELSE
		MessageLength := Buffer.EntryData[EntryDataIndexToBeReadNext].IndexOfLastByte - Buffer.EntryData[EntryDataIndexToBeReadNext].IndexOfFirstByte + 1 + (_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Bytes - 1);
		MessageLengthOfFirstPart := (_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Bytes - 1) - Buffer.EntryData[EntryDataIndexToBeReadNext].IndexOfFirstByte;
		
		// now populate the return
		stMsg.iLength := UDINT_TO_INT(MessageLength);
		MEMCPY(destAddr := ADR(stMsg.arrByte[0]), srcAddr := ADR(Buffer.Element[Buffer.EntryData[EntryDataIndexToBeReadNext].IndexOfFirstByte]), n := MessageLengthOfFirstPart);
		MEMCPY(destAddr := ADR(stMsg.arrByte[0 + MessageLengthOfFirstPart]), srcAddr := ADR(Buffer.Element[0]), n := (MessageLength - MessageLengthOfFirstPart));
	END_IF
END_IF

//return the result
GetHeadAsMsgRaw := stMsg;]]></ST>
      </Implementation>
    </Method>
    <Method Name="GetHeadAsPointerToByte" Id="{a2f6a5fe-820d-4bf7-bedb-88bc5bb46a55}">
      <Declaration><![CDATA[METHOD GetHeadAsPointerToByte : ST_MsgRaw
VAR_INPUT
END_VAR
VAR
	EntryDataIndexToBeReadNext: UDINT;
END_VAR
VAR_OUTPUT
	O_DataInNumberOfParts: UINT;
	O_PointerToByte_Part1: POINTER TO BYTE;
	O_DataLength_Part1: UDINT;
	O_PointerToByte_Part2: POINTER TO BYTE;
	O_DataLength_Part2: UDINT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[//get the oldest entry
IF Buffer.IndexOfLastReadEntryData < (_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Entries - 1) THEN // as we are interested in the NEXT byte after the last read message, we check whether the index of the next EntryData is 0 or (IndexofLastEntryData + 1)
	EntryDataIndexToBeReadNext := Buffer.IndexOfLastReadEntryData + 1;
ELSE
	EntryDataIndexToBeReadNext := 0;
END_IF

// initialise return
O_DataInNumberOfParts := 0;
O_DataLength_Part1 := 0;
O_DataLength_Part2 := 0;

// Create length information and pointers to data
IF Buffer.EntryData[EntryDataIndexToBeReadNext].writtenButNotRead THEN
	IF Buffer.EntryData[EntryDataIndexToBeReadNext].consecutiveMemorySpace THEN // depending on how the message is arranges in the buffer elements/ memory, we either can copy it as a whole at once, or must do a first and second part copy
		O_DataInNumberOfParts := 1;
		
		// get the message length
		O_DataLength_Part1 := Buffer.EntryData[EntryDataIndexToBeReadNext].IndexOfLastByte - Buffer.EntryData[EntryDataIndexToBeReadNext].IndexOfFirstByte + 1;
		
		// now populate the return
		O_PointerToByte_Part1 := ADR(Buffer.Element[Buffer.EntryData[EntryDataIndexToBeReadNext].IndexOfFirstByte]);
	ELSE
		O_DataInNumberOfParts := 2;
		O_DataLength_Part1 := (_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Bytes - 1) - Buffer.EntryData[EntryDataIndexToBeReadNext].IndexOfFirstByte;
		O_DataLength_Part2 := Buffer.EntryData[EntryDataIndexToBeReadNext].IndexOfLastByte - Buffer.EntryData[EntryDataIndexToBeReadNext].IndexOfFirstByte + 1 + (_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Bytes - 1) - O_DataLength_Part1;
		
		// now populate the return
		O_PointerToByte_Part1 := ADR(Buffer.Element[Buffer.EntryData[EntryDataIndexToBeReadNext].IndexOfFirstByte]);
		O_PointerToByte_Part2 := ADR(Buffer.Element[0]);
	END_IF
END_IF]]></ST>
      </Implementation>
    </Method>
    <Property Name="IsEmpty" Id="{f4c92557-9361-473c-9a1e-97759081a7d1}">
      <Declaration><![CDATA[PROPERTY IsEmpty : Bool ]]></Declaration>
      <Get Name="Get" Id="{0ce4ea61-b05d-414a-b632-bd7c4d496a9b}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IsEmpty := (BytesWaiting = 0) AND (EntriesWaiting = 0);
]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Method Name="RemoveHead" Id="{9ce6d166-c752-43f2-ae1d-7e76add3bafe}">
      <Declaration><![CDATA[METHOD RemoveHead
VAR_INPUT
END_VAR
VAR
	EntryDataIndexToBeReadNext: UDINT;
	tmpEntryData: ST_FB_ByteBuffer_Threadsafe_ByteBuffer_EntryData;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[// Update Statistics
messagesEverRemovedFromBuffer := messagesEverRemovedFromBuffer + 1;

IF Buffer.IndexOfLastReadEntryData < (_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Entries - 1) THEN // as we are interested in the NEXT byte after the last read message, we check whether the index of the next EntryData is 0 or (IndexofLastEntryData + 1)
	EntryDataIndexToBeReadNext := Buffer.IndexOfLastReadEntryData + 1;
ELSE
	EntryDataIndexToBeReadNext := 0;
END_IF

IF Buffer.EntryData[EntryDataIndexToBeReadNext].writtenButNotRead THEN
	Buffer.EntryData[EntryDataIndexToBeReadNext] := tmpEntryData;
END_IF

// Update MUST happen AFTER the data is written!
Buffer.IndexOfLastReadEntryData := EntryDataIndexToBeReadNext;
		
// Update State values
updateStates();]]></ST>
      </Implementation>
    </Method>
    <Method Name="updateStates" Id="{3a7f3c74-3c8c-499e-b030-9598c84718f5}">
      <Declaration><![CDATA[METHOD updateStates 
VAR_INPUT
END_VAR
VAR
	value: UDINT;
	IndexToBeReadNext: UDINT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[///
value := Buffer.IndexOfLastWrittenEntryData - Buffer.IndexOfLastReadEntryData;
IF Buffer.IndexOfLastWrittenEntryData < Buffer.IndexOfLastReadEntryData THEN
	value := value + (_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Entries - 1);
END_IF

DO_NOT_USE_EntriesWaiting := value;
///
DO_NOT_USE_EntriesAvailable := _Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Entries - EntriesWaiting;
///
///

IF EntriesWaiting > 0 THEN // if no entries are waiting in this buffer, no bytes are waiting as well!
	IF Buffer.IndexOfLastReadEntryData < (_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Entries - 1) THEN // as we are interested in the NEXT byte after the last read message, we check whether the index of the next EntryData is 0 or (IndexofLastEntryData + 1)
		IndexToBeReadNext := Buffer.IndexOfLastReadEntryData + 1;
	ELSE
		IndexToBeReadNext := 0;
	END_IF
	
	value := Buffer.EntryData[Buffer.IndexOfLastWrittenEntryData].IndexOfLastByte - Buffer.EntryData[IndexToBeReadNext].IndexOfFirstByte + 1; // if we start and end on the same byte, still one byte is waiting!
	
	IF Buffer.EntryData[Buffer.IndexOfLastWrittenEntryData].IndexOfLastByte < (Buffer.EntryData[IndexToBeReadNext].IndexOfFirstByte + 1) THEN // this means we last wrote to a Buffer.Element under the last read Element + 1
		value := value + (_Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Bytes - 1);
	//ELSIF value = 0 THEN // this is an exception: this means, we have NULL messages!
	//	;
	END_IF
	
	DO_NOT_USE_BytesWaiting := value;
ELSE
	DO_NOT_USE_BytesWaiting := 0;
END_IF
//
DO_NOT_USE_BytesAvailable := _Constant._C_ByteBuffer_Threadsafe_InnerBufferSize_Bytes - BytesWaiting;]]></ST>
      </Implementation>
    </Method>
    <LineIds Name="FB_InnerByteBuffer">
      <LineId Id="9" Count="0" />
    </LineIds>
    <LineIds Name="FB_InnerByteBuffer.AddTailAsMsgRaw">
      <LineId Id="158" Count="0" />
      <LineId Id="5" Count="59" />
      <LineId Id="171" Count="3" />
      <LineId Id="114" Count="2" />
      <LineId Id="65" Count="1" />
      <LineId Id="145" Count="0" />
      <LineId Id="143" Count="0" />
      <LineId Id="67" Count="16" />
      <LineId Id="144" Count="0" />
      <LineId Id="84" Count="17" />
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="FB_InnerByteBuffer.AddTailAsPointerToByte">
      <LineId Id="158" Count="0" />
      <LineId Id="6" Count="1" />
      <LineId Id="188" Count="0" />
      <LineId Id="8" Count="40" />
      <LineId Id="178" Count="0" />
      <LineId Id="49" Count="1" />
      <LineId Id="181" Count="1" />
      <LineId Id="185" Count="0" />
      <LineId Id="183" Count="0" />
      <LineId Id="186" Count="1" />
      <LineId Id="179" Count="0" />
      <LineId Id="51" Count="4" />
      <LineId Id="207" Count="0" />
      <LineId Id="56" Count="1" />
      <LineId Id="213" Count="0" />
      <LineId Id="209" Count="0" />
      <LineId Id="219" Count="0" />
      <LineId Id="226" Count="0" />
      <LineId Id="223" Count="0" />
      <LineId Id="227" Count="0" />
      <LineId Id="224" Count="0" />
      <LineId Id="222" Count="0" />
      <LineId Id="228" Count="0" />
      <LineId Id="221" Count="0" />
      <LineId Id="225" Count="0" />
      <LineId Id="214" Count="0" />
      <LineId Id="229" Count="6" />
      <LineId Id="238" Count="0" />
      <LineId Id="237" Count="0" />
      <LineId Id="217" Count="0" />
      <LineId Id="216" Count="0" />
      <LineId Id="210" Count="1" />
      <LineId Id="208" Count="0" />
      <LineId Id="59" Count="5" />
      <LineId Id="254" Count="3" />
      <LineId Id="114" Count="2" />
      <LineId Id="65" Count="1" />
      <LineId Id="145" Count="0" />
      <LineId Id="143" Count="0" />
      <LineId Id="67" Count="16" />
      <LineId Id="144" Count="0" />
      <LineId Id="84" Count="17" />
      <LineId Id="192" Count="0" />
      <LineId Id="195" Count="11" />
      <LineId Id="189" Count="0" />
      <LineId Id="191" Count="0" />
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="FB_InnerByteBuffer.BytesAvailable.Get">
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="FB_InnerByteBuffer.BytesWaiting.Get">
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="FB_InnerByteBuffer.Clear">
      <LineId Id="3" Count="0" />
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="FB_InnerByteBuffer.EntriesAvailable.Get">
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="FB_InnerByteBuffer.EntriesWaiting.Get">
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="FB_InnerByteBuffer.GetHeadAsMsgRaw">
      <LineId Id="3" Count="28" />
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="FB_InnerByteBuffer.GetHeadAsPointerToByte">
      <LineId Id="3" Count="0" />
      <LineId Id="6" Count="3" />
      <LineId Id="48" Count="4" />
      <LineId Id="10" Count="0" />
      <LineId Id="53" Count="0" />
      <LineId Id="11" Count="2" />
      <LineId Id="54" Count="1" />
      <LineId Id="14" Count="3" />
      <LineId Id="19" Count="1" />
      <LineId Id="56" Count="2" />
      <LineId Id="23" Count="1" />
      <LineId Id="26" Count="2" />
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="FB_InnerByteBuffer.IsEmpty.Get">
      <LineId Id="3" Count="0" />
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="FB_InnerByteBuffer.RemoveHead">
      <LineId Id="33" Count="0" />
      <LineId Id="32" Count="0" />
      <LineId Id="31" Count="0" />
      <LineId Id="3" Count="10" />
      <LineId Id="21" Count="2" />
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="FB_InnerByteBuffer.updateStates">
      <LineId Id="77" Count="0" />
      <LineId Id="82" Count="2" />
      <LineId Id="40" Count="1" />
      <LineId Id="35" Count="0" />
      <LineId Id="43" Count="0" />
      <LineId Id="67" Count="2" />
      <LineId Id="44" Count="16" />
      <LineId Id="62" Count="1" />
      <LineId Id="42" Count="0" />
      <LineId Id="65" Count="1" />
    </LineIds>
  </POU>
</TcPlcObject>