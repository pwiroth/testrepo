﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.6">
  <POU Name="Visu_EStoFillTable" Id="{79f67d85-af1d-401b-a389-5aebe60be0e3}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION Visu_EStoFillTable : INT

VAR_INPUT
	I_CalledByBlock:	ST_Block;
END_VAR

VAR
	Block:	ST_Block;
	Index:			INT;
	Index2:			INT;
	Line:			INT;
	ResetLine:		INT;
	EstopAreaLine:	INT;
	sTmpString:	STRING(_Constant._C_LstrLen);
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[Block.Name := 'Visu.EStoFillTable';

// INIT

// CONFIG

// MAIN

//E-Stop Table
Line := -1;
ResetLine := -1;
EstopAreaLine:= -1;

FOR Index:= 0 TO _visu._C_MaxElementEStoIndex DO
	IF _Visu.vESto.Objects[Index].config.Exists  THEN

		IF _Visu.vESto.Objects[Index].config.EstopType = EN_Visu_EStopType.EStop AND
		_Visu.vESto.Objects[Index].HasError = TRUE  THEN
			IF _visu.vESto.IO.TableMaxLines < _C_MaxCntOfESTopTableLines THEN
				Line := Line + 1;
				_Visu.vESto.IO.Table[0,Line] := _Visu.vESto.Objects[index].config.Alias;		
				_Visu.vESto.IO.Table[1,Line] := _Visu.vESto.Objects[index].config.GroupDesig;	
				_Visu.vESto.IO.Table[2,Line] := _Visu.vESto.Objects[index].Desig;
				_Visu.vESto.IO.Table[3,Line] := _Visu.vESto.Objects[index].Ram.ErrorText;
			ELSE
				sTmpString := 'Can not show more EStops in table then:  ';
				sTmpString := CONCAT(STR1 := sTmpString, STR2 := INT_TO_STRING(_C_MaxCntOfESTopTableLines));
				WriteLogMsg(
					I_Block			:= Block,
					I_CalledByBlock	:= I_CalledByBlock,
					I_MsgLevel		:= EN_LogLevel.Error,
					I_ItemUID		:= -1,
					I_ConUID		:= -1,
					I_ComPUID		:= -1,
					I_ConvUID		:= -1,
					I_CarrierUID	:= -1,
					I_DropUID		:= -1,
					I_Msg			:= sTmpString
					);
			END_IF			
		ELSIF _Visu.vESto.Objects[Index].config.EstopType = EN_Visu_EStopType.EStopReset AND
		_Visu.vESto.Objects[Index].HasError = TRUE THEN
			IF _visu.vESto.IO.TableMaxLines < _C_MaxCntOfESTopResetTableLines THEN
				ResetLine := ResetLine + 1;
				_Visu.vESto.IO.ResetTable[0,ResetLine] := _Visu.vESto.Objects[index].config.Alias;		
				_Visu.vESto.IO.ResetTable[1,ResetLine] := _Visu.vESto.Objects[index].config.GroupDesig;	
				_Visu.vESto.IO.ResetTable[2,ResetLine] := _Visu.vESto.Objects[index].Desig;
				_Visu.vESto.IO.ResetTable[3,ResetLine] := _Visu.vESto.Objects[index].Ram.ErrorText;
			ELSE
				sTmpString := 'Can not show more EStop-Resets in table then:  ';
				sTmpString := CONCAT(STR1 := sTmpString, STR2 := INT_TO_STRING(_C_MaxCntOfESTopResetTableLines));
				WriteLogMsg(
					I_Block			:= Block,
					I_CalledByBlock	:= I_CalledByBlock,
					I_MsgLevel		:= EN_LogLevel.Error,
					I_ItemUID		:= -1,
					I_ConUID		:= -1,
					I_ComPUID		:= -1,
					I_ConvUID		:= -1,
					I_CarrierUID	:= -1,
					I_DropUID		:= -1,
					I_Msg			:= sTmpString
					);
			END_IF
		ELSIF _Visu.vESto.Objects[Index].config.EstopType = EN_Visu_EStopType.EstopArea AND
		_Visu.vESto.Objects[Index].HasAreaStop = TRUE THEN
			IF _visu.vESto.IO.TableMaxLines < _C_MaxCntOfESTopAreaTableLines THEN
				EstopAreaLine := EstopAreaLine + 1;
				_Visu.vESto.IO.EstopAreaTable[0,EstopAreaLine] := _Visu.vESto.Objects[index].config.GroupDesig;		
				_Visu.vESto.IO.EstopAreaTable[1,EstopAreaLine] := _Visu.vESto.Objects[index].config.Alias;
			ELSE
				sTmpString := 'Can not show more EStop-Areas in table then:  ';
				sTmpString := CONCAT(STR1 := sTmpString, STR2 := INT_TO_STRING(_C_MaxCntOfESTopAreaTableLines));
				WriteLogMsg(
					I_Block			:= Block,
					I_CalledByBlock	:= I_CalledByBlock,
					I_MsgLevel		:= EN_LogLevel.Error,
					I_ItemUID		:= -1,
					I_ConUID		:= -1,
					I_ComPUID		:= -1,
					I_ConvUID		:= -1,
					I_CarrierUID	:= -1,
					I_DropUID		:= -1,
					I_Msg			:= sTmpString
					);
			END_IF
		END_IF
	END_IF
END_FOR

//Update Values
_Visu.vESto.IO.TableMaxLines := Line;
_Visu.vESto.IO.ResetTableMaxLines := ResetLine;
_Visu.vESto.IO.EstopAreaMaxLines := EstopAreaLine;

//For clean display purposes, when change to -1 is detected, delete all entries
IF _Visu.vESto.RAM.TableMaxLines <> _Visu.vESto.IO.TableMaxLines AND _Visu.vESto.IO.TableMaxLines < 0 THEN
	FOR Index := 0 TO _Visu._C_MaxCntOfEStopTableColums BY 1 DO
		FOR Index2 := 0 TO _Visu._C_MaxCntOfEStopTableLines BY 1 DO
			_Visu.vESto.IO.Table[Index,Index2] := '';
		END_FOR
	END_FOR
END_IF

IF _Visu.vESto.RAM.ResetTableMaxLines <> _Visu.vESto.IO.ResetTableMaxLines AND _Visu.vESto.IO.ResetTableMaxLines < 0 THEN
	FOR Index := 0 TO _Visu._C_MaxCntOfEStopResetTableColums BY 1 DO
		FOR Index2 := 0 TO _Visu._C_MaxCntOfEStopResetTableLines BY 1 DO
			_Visu.vESto.IO.ResetTable[Index,Index2] := '';
		END_FOR
	END_FOR
END_IF

IF _Visu.vESto.RAM.EstopAreaMaxLines <> _Visu.vESto.IO.EstopAreaMaxLines AND _Visu.vESto.IO.EstopAreaMaxLines < 0 THEN
	FOR Index := 0 TO _Visu._C_MaxCntOfEStopAreaTableColums BY 1 DO
		FOR Index2 := 0 TO _Visu._C_MaxCntOfEStopAreaTableLines BY 1 DO
			_Visu.vESto.IO.EstopAreaTable[Index,Index2] := '';
		END_FOR
	END_FOR
END_IF

//Save old values in RAM
_Visu.vESto.RAM.TableMaxLines := _Visu.vESto.IO.TableMaxLines;
_Visu.vESto.RAM.ResetTableMaxLines := _Visu.vESto.IO.ResetTableMaxLines;
_Visu.vESto.RAM.EstopAreaMaxLines := _Visu.vESto.IO.EstopAreaMaxLines;
]]></ST>
    </Implementation>
    <LineIds Name="Visu_EStoFillTable">
      <LineId Id="3" Count="123" />
      <LineId Id="2" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>