@echo off
@echo ^<?xml version="1.0" encoding="utf-8"?^>> Framework\Framework\GVLs\_CurrentCommitHash.TcGVL
@echo ^<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.6"^>>> Framework\Framework\GVLs\_CurrentCommitHash.TcGVL
@echo   ^<GVL Name="_CurrentCommitHash" Id="{87b1e0ba-ae16-4b9e-9f0c-85c229526f33}"^>>> Framework\Framework\GVLs\_CurrentCommitHash.TcGVL
@echo     ^<Declaration^>^<![CDATA[{attribute 'qualified_only'}>> Framework\Framework\GVLs\_CurrentCommitHash.TcGVL
@echo VAR_GLOBAL>> Framework\Framework\GVLs\_CurrentCommitHash.TcGVL
@echo 	_CurrentCommitHash: STRING := '%1';>> Framework\Framework\GVLs\_CurrentCommitHash.TcGVL
@echo 	_CurrentCommitDate: STRING := '%2';>> Framework\Framework\GVLs\_CurrentCommitHash.TcGVL
@echo END_VAR]]^>^</Declaration^>>> Framework\Framework\GVLs\_CurrentCommitHash.TcGVL
@echo   ^</GVL^>>> Framework\Framework\GVLs\_CurrentCommitHash.TcGVL
@echo ^</TcPlcObject^>>> Framework\Framework\GVLs\_CurrentCommitHash.TcGVL